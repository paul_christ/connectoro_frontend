import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import MenuComponent from "./components/menu/MenuComponent/MenuComponent";
import ContentComponent from "./components/content/ContentComponent/ContentComponent";
import requiresAuth from "./components/authentication/AuthenticatedComponent/AuthenticatedComponent";
import Login from "./views/Login/Login";
import Register from "./views/Register/Register";
import "./App.css";

const [AuthenticatedMenuComponent, AuthenticatedContentComponent] = [MenuComponent, ContentComponent].map((component) =>
    requiresAuth(component),
);

interface AppState {
    config: object;
}

class App extends Component<{}, AppState> {
    constructor(readonly props: object) {
        super(props);

        this.state = {
            config: {},
        };
    }

    async componentDidMount(): Promise<void> {
        const response = await fetch("config.json");
        const config = await response.json();
        return this.setState({ config });
    }

    render(): JSX.Element {
        return (
            <div className="app">
                <Router>
                    <AuthenticatedMenuComponent />
                    <AuthenticatedContentComponent />
                    <Route path="/login" component={Login} />
                    <Route path="/register" component={Register} />
                </Router>
            </div>
        );
    }
}

export default App;
