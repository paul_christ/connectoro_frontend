import React, { Component } from "react";
import "./LandingPage.css";

export class LandingPage extends Component {
    render(): JSX.Element {
        return (
            <div className="landing">
                <div className="landing__header">
                    <h1 className="landing__header--title">Willkommen</h1>
                    <hr className="landing__header--rule" />
                </div>
                <div className="landing__content">
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Vero amet natus totam delectus nisi quam mollitia id ex
                    voluptate itaque? Hic quidem recusandae labore atque, illo necessitatibus esse dolores natus!
                </div>
            </div>
        );
    }
}

export default LandingPage;
