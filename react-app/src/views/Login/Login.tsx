import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import FloatLabel from "../../components/ui-lib/FloatLabel";
import Button from "../../components/ui-lib/Button";
import "./Login.css";

interface LoginState {
    redirect: boolean;
    location: string;
}

export class Login extends Component<{}, LoginState> {
    constructor(readonly props: object) {
        super(props);

        this.state = { redirect: false, location: "" };
    }

    componentDidMount(): void {
        this.checkAndRedirect();
    }

    checkAndRedirect(): void {
        const email = localStorage.getItem("email");
        const password = localStorage.getItem("password");

        if (email || password) {
            this.setState({ redirect: true, location: "/" });
        }
    }

    register: React.EventHandler<React.SyntheticEvent> = () => {
        this.setState({ redirect: true, location: "/register" });
    };

    login: React.EventHandler<React.SyntheticEvent> = () => {
        const email = (document.querySelector("#E-Mail") as HTMLInputElement).value;
        const password = (document.querySelector("#Password") as HTMLInputElement).value;

        if (password && email) {
            localStorage.setItem("email", email);
            localStorage.setItem("password", password);
            this.setState({ redirect: true, location: "/" });
        }
    };

    render(): JSX.Element {
        if (this.state.redirect) {
            return <Redirect to={this.state.location} />;
        }
        return (
            <div className="login">
                <div className="login__box">
                    <h1 className="login__box--header">Bei SIS anmelden</h1>
                    <div className="login__box--form">
                        <FloatLabel label="E-Mail" type="email" />
                        <FloatLabel label="Password" type="password" />
                        <Button name="Anmelden" type="action" eventHandler={this.login} />
                        <Button name="Nutzerkonto erstellen" eventHandler={this.register} />
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;
