import React, { Component } from "react";
import FloatLabel from "../../components/ui-lib/FloatLabel";
import Button from "../../components/ui-lib/Button";
import "./Settings.css";

interface SettingsObject {
    [email: string]: string;
    password: string;
}

interface LoginState {
    email: string;
}

export class Settings extends Component<{}, LoginState> {
    constructor(readonly props: object) {
        super(props);
        this.state = {
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            email: localStorage.getItem("email")!,
        };
    }

    saveSettings: React.EventHandler<React.SyntheticEvent> = () => {
        const settings: SettingsObject = {
            email: (document.querySelector("#E-Mail-Adresse") as HTMLInputElement).value,
            password: (document.querySelector("#Passwort\\ ändern") as HTMLInputElement).value,
        };
        Object.keys(settings).forEach((setting: string) => {
            if (settings.hasOwnProperty(setting) && settings[setting]) {
                localStorage.setItem(setting, settings[setting]);
            }
        });
    };

    render(): JSX.Element {
        return (
            <div className="settings">
                <div className="settings__header">
                    <h1 className="settings__header--title">Einstellungen</h1>
                    <hr className="settings__header--rule" />
                </div>
                <div className="settings__options">
                    <FloatLabel label="E-Mail-Adresse" type="email" value={this.state.email} />
                    <FloatLabel label="Passwort ändern" type="password" />
                    <FloatLabel label="Passwort bestätigen" type="password" />
                </div>

                <Button name="Speichern" type="action" eventHandler={this.saveSettings} />
            </div>
        );
    }
}

export default Settings;
