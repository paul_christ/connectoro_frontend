import React, { Component } from "react";
import "./Button.css";

interface ButtonProps {
    eventHandler: React.EventHandler<React.SyntheticEvent>;
    name: string;
    type?: string;
}

export class Button extends Component<ButtonProps, {}> {
    render(): JSX.Element {
        const { name, eventHandler } = this.props;
        let { type } = this.props;
        type = type || "default";

        return (
            <div className={`button__${name}--${type}`} onClick={eventHandler}>
                {name}
            </div>
        );
    }
}

export default Button;
