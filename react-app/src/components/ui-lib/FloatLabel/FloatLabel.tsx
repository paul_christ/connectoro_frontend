import React, { Component } from "react";
import "./FloatLabel.css";

interface FloatLabelProps {
    label: string;
    type?: string;
    value?: string;
}

export class FloatLabel extends Component<FloatLabelProps, {}> {
    render(): JSX.Element {
        let { type, value } = this.props;
        const { label } = this.props;
        type = type || "text";
        value = value || "";

        return (
            <div className="floatingLabel">
                <input id={label} type={type} defaultValue={value} className="floatingLabel__input" required />
                <label className="floatingLabel__label" htmlFor={label}>
                    {label}
                </label>
            </div>
        );
    }
}

export default FloatLabel;
