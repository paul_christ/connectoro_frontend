import React, { Component, EventHandler } from "react";
import { Link } from "react-router-dom";
import "./MenuComponent.css";

interface NavObject {
    routes: Array<Route>;
    isCollapsed: boolean;
    collapse: EventHandler<React.SyntheticEvent>;
    active: Function;
    isActive: number;
}

const Nav: Function = ({ routes, isCollapsed, collapse, active, isActive }: NavObject) => (
    <nav className={`nav ${isCollapsed ? "collapsed" : ""}`}>
        <div className="nav__header">
            <div className="nav__header--icon" onClick={collapse}>
                &#10148;
            </div>
        </div>
        <div className="nav__content">
            {routes.map((route: Route, index: number) => (
                <Link
                    className={`nav__item nav__item--${index} ${isActive === index ? "active" : ""}`}
                    key={route.url}
                    onClick={active(index)}
                    to={route.url}
                >
                    <img className="nav__item--icon" src={route.icon} alt={route.name} />
                    <div className="nav__item--link">{route.name}</div>
                </Link>
            ))}
        </div>
    </nav>
);

interface Route {
    url: string;
    icon: string;
    name: string;
}

const routes: Array<Route> = [
    {
        url: "/",
        icon: "/images/icons/home-icon.jpg",
        name: "Home",
    },
    {
        url: "/models",
        icon: "/images/icons/document-icon.jpg",
        name: "Kommunikationsmodelle",
    },
    {
        url: "/settings",
        icon: "/images/icons/settings-icon.png",
        name: "Einstellungen",
    },
];

interface MenuState {
    isCollapsed: boolean;
    isActive: number;
}

export class MenuComponent extends Component<{}, MenuState> {
    constructor(readonly props: object) {
        super(props);

        this.state = { isCollapsed: false, isActive: 0 };
    }

    collapse: Function = () => {
        this.setState({ isCollapsed: !this.state.isCollapsed });
    };

    active: Function = (id: number) => (): void => {
        this.setState({ isActive: id });
    };

    render(): JSX.Element {
        return (
            <Nav
                routes={routes}
                isCollapsed={this.state.isCollapsed}
                collapse={this.collapse}
                active={this.active}
                isActive={this.state.isActive}
            />
        );
    }
}

export default MenuComponent;
