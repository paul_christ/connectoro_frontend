import React, { Component } from "react";
import { Redirect } from "react-router-dom";

interface AuthenticationState {
    redirect: boolean;
}

const requiresAuth: Function = (WrappedComponent: React.ComponentType) => {
    return class AuthenticatedComponent extends Component<{}, AuthenticationState> {
        constructor(readonly props: object) {
            super(props);

            this.state = {
                redirect: false,
            };
        }

        componentDidMount(): void {
            this.checkAndRedirect();
        }

        checkAndRedirect(): void {
            const email = localStorage.getItem("email");
            const password = localStorage.getItem("password");

            if (!email || !password) {
                this.setState({ redirect: true });
            }
        }

        render(): JSX.Element {
            if (this.state.redirect) {
                return <Redirect to="/login" />;
            }

            return <WrappedComponent {...this.props} />;
        }
    };
};

export default requiresAuth;
