import React, { Component } from "react";
import { Route } from "react-router-dom";
import LandingPage from "../../../views/LandingPage";
import CommunicationModels from "../../../views/CommunicationModels";
import Settings from "../../../views/Settings";

export class ContentComponent extends Component {
    render(): JSX.Element {
        return (
            <div className="app__content">
                <Route exact path="/" component={LandingPage} />
                <Route path="/models" component={CommunicationModels} />
                <Route path="/settings" component={Settings} />
            </div>
        );
    }
}

export default ContentComponent;
