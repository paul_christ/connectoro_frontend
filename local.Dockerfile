FROM node

WORKDIR /usr/src/app

ENV PATH /usr/src/app/node_modules/.bin:$PATH

COPY ./react-app/package.json ./

#remove silent flags for debugging
RUN apt-get update
RUN apt-get install git -y
RUN npm install typescript
RUN npm install --silent
RUN npm install react-scripts -g --silent
RUN npm update
RUN npm audit fix

CMD npm start
